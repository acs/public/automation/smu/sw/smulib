// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "subcfg.h"
#include <QTextStream>

static const char* TAG = "subCFG";

subCFG::subCFG(QObject *parent)
    : QObject(parent)
{
    error.svc = SMU_NO_INIT;
    error.daq = SMU_NO_INIT;
    error.dsp = SMU_NO_INIT;
    error.net = SMU_NO_INIT;

    daq = daq_cfg_default;
    dsp = dsp_cfg_default;
    net = net_cfg_default;

    //    file = new QFile(QCoreApplication::applicationDirPath() + "/" + SMU_CFG);
    //    file = new QFile(SMU_CFG);
    file = new QFile(SMU_CFG);
}

void subCFG::load()
{
    // Load configuration from local file
    if (!file->exists()){
        SMU_LOGW(TAG,"[%1] config file not available.");
        SMU_LOGI(TAG,"[%1] using default configuration.");
        return;
    }

    // Open config file
    if(!file->open(QIODevice::ReadOnly | QIODevice::Text)){
        SMU_LOGE(TAG,"[%1] fail to open config file->");
        SMU_LOGI(TAG,"[%1] using default configuration.");
        return;
    }

    // Read config from file
    QString config(file->readAll());
    fromString(config);

    // Close config file
    file->close();

    SMU_LOGI(TAG,"[%1] config file loaded.");
}

void subCFG::save()
{
    // Open config file
    if (!file->open(QIODevice::WriteOnly | QIODevice::Text)){
        SMU_LOGW(TAG,"[%1] fail to save config file->");
        return;
    }

    // Write config to file
    QTextStream stream(file);
    toString(config);
    stream << config;

    // Close config file
    file->flush();
    file->close();

    SMU_LOGI(TAG,"[%1] config file saved.");
}

void subCFG::toString(QString &str)
{
    str.clear();
    QTextStream out(&str);

    out << "# DAQ module"                        << endl;
    out << "fsamp:"    << QString::number(daq.rate) << endl;
    out << "daq_mode:"  << QString::number(daq.mode) << endl;
    out << "sync:"  << QString::number(daq.sync) << endl;
    out << "# DSP module"                        << endl;
    out << "frep:"    << QString::number(dsp.rate) << endl;
    out << "state:" << dsp.state.join(',')       << endl;
    out << "alias:" << dsp.alias.join(',')       << endl;
    out << "core:"  << QString(dsp.core)         << endl;
    out << "# NET module"                        << endl;
    out << "device:"<< net.devID                 << endl;
    out << "ihost:" << net.ihost                 << endl;
    out << "iport:" << QString::number(net.iport)<< endl;
    out << "iprot:" << QString::number(net.iprot)<< endl;
    out << "itran:" << QString::number(net.itran)<< endl;
    out << "iappl:" << net.iappl                 << endl;
    out << "ohost:" << net.ohost                 << endl;
    out << "oport:" << QString::number(net.oport)<< endl;
    out << "oprot:" << QString::number(net.oprot)<< endl;
    out << "otran:" << QString::number(net.otran)<< endl;
    out << "oappl:" << net.oappl                 << endl;
    out << "oargs:" << net.oargs                 << endl;
    out << "# DATALOG configuration"             << endl;
    out << "datalog_mode:"           << QString::number(datalog.datalog_mode)    << endl;
    out << "route:"             << datalog.route                            << endl;
    out << "file_close_time:"   << QString::number(datalog.duration)        << endl;

    for (int i=1; i<5;i++)
    {
        out << "# PARAMETER " << QString::number(i)<<" GAIN COEFFICIENTS configuration" << endl;
        for (int ch=0;ch<8;ch++)
        {
            out <<"p"<<QString::number(i)<<"_gain_ch"<<QString::number(ch+1)<<":";
            switch (i)
            {
                case 1:
                    out<< QString::number(p1.ch[ch].gain) << endl;
                    break;
                case 2:
                    out<< QString::number(p2.ch[ch].gain) << endl;
                    break;
                case 3:
                    out<< QString::number(p3.ch[ch].gain) << endl;
                    break;
                case 4:
                    out<< QString::number(p4.ch[ch].gain) << endl;
                    break;
            }

        }
    }
    for (int i=1; i<5;i++)
    {
        out << "# PARAMETER " << QString::number(i)<<" OFFSET VALUES configuration" << endl;
        for (int ch=0;ch<8;ch++)
        {
            out <<"p"<<QString::number(i)<<"_offset_ch"<<QString::number(ch+1)<<":";
            switch (i)
            {
                case 1:
                    out<< QString::number(p1.ch[ch].offset) << endl;
                    break;
                case 2:
                    out<< QString::number(p2.ch[ch].offset) << endl;
                    break;
                case 3:
                    out<< QString::number(p3.ch[ch].offset) << endl;
                    break;
                case 4:
                    out<< QString::number(p4.ch[ch].offset) << endl;
                    break;
            }

        }
    }
    for (int i=1; i<5;i++)
    {
        out << "# PARAMETER " << QString::number(i)<<" FREQUENCY-DRIFT VALUE compensation" << endl;
        out << "# (only done to the first sample of the second reported)" << endl;
        out <<"p"<<QString::number(i)<<"_f_drift_gain:";
        switch (i)
        {
            case 1:
                out<< QString::number(p1.f_drift.gain) << endl;
                break;
            case 2:
                out<< QString::number(p2.f_drift.gain) << endl;
                break;
            case 3:
                out<< QString::number(p3.f_drift.gain) << endl;
                break;
            case 4:
                out<< QString::number(p4.f_drift.gain) << endl;
                break;
        }
        out <<"p"<<QString::number(i)<<"_f_drift_offset:";
        switch (i)
        {
            case 1:
                out<< QString::number(p1.f_drift.offset) << endl;
                break;
            case 2:
                out<< QString::number(p2.f_drift.offset) << endl;
                break;
            case 3:
                out<< QString::number(p3.f_drift.offset) << endl;
                break;
            case 4:
                out<< QString::number(p4.f_drift.offset) << endl;
                break;
        }
    }
}

void subCFG::fromString(QString &str)
{
    QStringList list = str.split('\n');

    int j=0;

    for (int i=0; i<list.size(); i++){

        if (list.at(i).contains("#"))
            continue;

        if (list.at(i).contains("fsamp",Qt::CaseInsensitive))
            daq.rate = (daq_rate_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("daq_mode",Qt::CaseInsensitive))
            daq.mode = (daq_mode_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("sync",Qt::CaseInsensitive))
            daq.sync = (daq_sync_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("frep",Qt::CaseInsensitive))
            dsp.rate = (dsp_rate_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("state",Qt::CaseInsensitive))
            dsp.state = list.at(i).section(':',1,1).split(',');

        if (list.at(i).contains("alias",Qt::CaseInsensitive))
            dsp.alias = list.at(i).section(':',1,1).split(',');

        if (list.at(i).contains("core",Qt::CaseInsensitive))
            dsp.core = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("device:",Qt::CaseInsensitive))
            net.devID = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("ihost",Qt::CaseInsensitive))
            net.ihost = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("iport",Qt::CaseInsensitive))
            net.iport= list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("iprot",Qt::CaseInsensitive))
            net.iprot= (net_L2_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("itran",Qt::CaseInsensitive))
            net.itran= (net_L3_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("iappl",Qt::CaseInsensitive))
            net.iappl = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("ohost",Qt::CaseInsensitive))
            net.ohost = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("oport",Qt::CaseInsensitive))
            net.oport= list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("oprot",Qt::CaseInsensitive))
            net.oprot= (net_L2_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("otran",Qt::CaseInsensitive))
            net.otran= (net_L3_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("oappl",Qt::CaseInsensitive))
            net.oappl = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("oargs",Qt::CaseInsensitive))
            net.oargs = list.at(i).section(':',1,1).trimmed();

        if (list.at(i).contains("datalog_mode",Qt::CaseInsensitive))
            datalog.datalog_mode = (datalog_mode_t)list.at(i).section(':',1,1).toInt();

        if (list.at(i).contains("route",Qt::CaseInsensitive))
            datalog.route = list.at(i).section(':',1,1).trimmed();
        
        if (list.at(i).contains("file_close_time",Qt::CaseInsensitive))
            datalog.duration = list.at(i).section(':',1,1).toFloat();
        
        if (list.at(i).contains("gain_ch",Qt::CaseInsensitive))
        {   
            if (list.at(i).contains("p1",Qt::CaseInsensitive))         
                p1.ch[j].gain = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p2",Qt::CaseInsensitive))         
                p2.ch[j].gain = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p3",Qt::CaseInsensitive))         
                p3.ch[j].gain = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p4",Qt::CaseInsensitive))         
                p4.ch[j].gain = list.at(i).section(':',1,1).toFloat();
            j++;
            if (j==8)
                j=0;
        }                
        
        if (list.at(i).contains("offset_ch",Qt::CaseInsensitive))
        {
            if (list.at(i).contains("p1",Qt::CaseInsensitive))         
                p1.ch[j].offset = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p2",Qt::CaseInsensitive))         
                p2.ch[j].offset = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p3",Qt::CaseInsensitive))         
                p3.ch[j].offset = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p4",Qt::CaseInsensitive))         
                p4.ch[j].offset = list.at(i).section(':',1,1).toFloat();
            j++;
            if (j==8)
                j=0;
        }
        
        if (list.at(i).contains("f_drift_gain",Qt::CaseInsensitive))
        {
            if (list.at(i).contains("p1",Qt::CaseInsensitive))         
                p1.f_drift.gain = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p2",Qt::CaseInsensitive))         
                p2.f_drift.gain = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p3",Qt::CaseInsensitive))         
                p3.f_drift.gain = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p4",Qt::CaseInsensitive))         
                p4.f_drift.gain = list.at(i).section(':',1,1).toFloat();
        }
        if (list.at(i).contains("f_drift_offset",Qt::CaseInsensitive))
        {
            if (list.at(i).contains("p1",Qt::CaseInsensitive))         
                p1.f_drift.offset = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p2",Qt::CaseInsensitive))         
                p2.f_drift.offset = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p3",Qt::CaseInsensitive))         
                p3.f_drift.offset = list.at(i).section(':',1,1).toFloat();
            if (list.at(i).contains("p4",Qt::CaseInsensitive))         
                p4.f_drift.offset = list.at(i).section(':',1,1).toFloat();
        }
    }
	
	// Update submodules configuration
    print();
	emit updated();
}

void subCFG::print()
{
    toString(config);
    qDebug() << config;
}

void subCFG::reportConf(QString &str)
{
    str.clear();
    QTextStream out(&str);
    out << "\"sampling_freq\":"             << QString::number(daq.rate*1000)   << ","   <<  endl;
    out << "\"synch_source\":";
    switch(daq.sync) {
        case SYNC_NONE:
            out << "none,"  << endl;
        break;
        case SYNC_PPS:
            out << "pps,"           << endl;
        break;
        case SYNC_NTP:
            out << "ntp,"           << endl;
        break;
        default:
            out << "ptp,"           << endl;
    }
    out << "\"report_freq\":"           << QString::number(dsp.rate)            << ","   << endl;
    out << "\"dsp_method\":"           << dsp.core                              << ",";
}
