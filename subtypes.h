// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SUBTYPES_H
#define SUBTYPES_H

#include <QString>
#include <QStringList>


/// ---------- SMU: DAQ settings section ----------

/**
 * @brief DAQ sampling rate (kHz / kSPS)
 *
 */
typedef enum : unsigned char
{
    FS_1kSPS   =   1,
    FS_2kSPS   =   2,
    FS_5kSPS   =   5,
    FS_10kSPS  =  10,
    FS_20kSPS  =  20,
    FS_25kSPS  =  25,
    FS_50kSPS  =  50,
    FS_100kSPS = 100,
    FS_200kSPS = 200
} daq_rate_t;

/**
 * @brief DAQ buffer frame rate (Hz / FPS)
 *
 */
typedef enum : unsigned char
{
    FB_10FPS  =  10,
    FB_20FPS  =  20,
    FB_50FPS  =  50,
    FB_100FPS = 100,
    FB_200FPS = 200
} daq_buff_t;

/**
 * @brief DAQ acquisition mode
 *
 */
typedef enum : unsigned char
{
    MODE_ONESHOT,                   /*!< DAQ runs for 1 second */
    MODE_FREERUN,                   /*!< DAQ runs continuously */
    MODE_VIRTUAL,                   /*!< DAQ runs virtually */
    MODE_MAX
} daq_mode_t;

/**
 * @brief DAQ synchronization mode
 *
 */
typedef enum : unsigned char
{
    SYNC_NONE,                      /*!< Time base synchronization disabled */
    SYNC_PPS,                       /*!< Time base synchronization from GPS via pulse-per-second */
    SYNC_NTP,                       /*!< Time base synchronization from Server via network-time-protocol */
    SYNC_PTP                        /*!< Time base synchronization from Server via precision-time-protocol */
} daq_sync_t;

/**
 * @brief DAQ parameters
 *
 */
typedef struct
{
    daq_rate_t rate;                /*!< DAQ sampling rate */
    daq_buff_t buff;                /*!< DAQ buffer frame rate */
    daq_mode_t mode;                /*!< DAQ acquisition mode */
    daq_sync_t sync;                /*!< DAQ synchronization mode */
} __attribute__((__packed__)) smu_daq_t;

/// ---------- SMU: DSP settings section ----------

/**
 * @brief DSP reporting rate (Hz / PPS)
 *
 */
typedef enum : unsigned short int
{
    FR_EVENT  = 0,
    FR_1PPS   = 1,
    FR_2PPS   = 2,
    FR_5PPS   = 5,
    FR_10PPS  = 10,
    FR_20PPS  = 20,
    FR_25PPS  = 25,
    FR_50PPS  = 50,
    FR_100PPS = 100
} dsp_rate_t;

/**
 * @brief DSP parameters
 *
 */
typedef struct
{
    dsp_rate_t  rate;               /*!< DSP reporting rate */
    QStringList state;              /*!< DSP channels state */
    QStringList alias;              /*!< DSP channels alias */
    QString core;                   /*!< DSP processing library */
} smu_dsp_t;


/// ---------- SMU: NET settings section ----------

/**
 * @brief TCP/IP Internet layer
 *
 */
typedef enum : unsigned short int
{
    IPv4,
    IPv6,
    AnyIP
} net_L2_t;

/**
 * @brief TCP/IP Transport layer
 *
 */
typedef enum : unsigned short int
{
    TCP,
    UDP
} net_L3_t;

/**
 * @brief DATALOG mode
 *
 */
typedef enum : unsigned short int
{
    MODE_DATALOG_OFF,                       /*!< DATALOG off */
    MODE_DATALOG_NO_COMM,                   /*!< DATALOG runs in case of disconnected communication */
    MODE_DATALOG_ALWAYS                     /*!< DATALOG runs continuously */
}   datalog_mode_t;

/**
 * @brief NET parameters
 *
 */
typedef struct
{
    QString  devID;                 /*!< Device ID */

    // Local host
    QString  ihost;                 /*!< Host address*/
    uint16_t iport;                 /*!< Host port*/
    net_L2_t iprot;                 /*!< Host internet layer protocol*/
    net_L3_t itran;                 /*!< Host transport layer protocol*/
    QString  iappl;                 /*!< Host application layer protocol*/

    // Remote host
    QString  ohost;
    uint16_t oport;
    net_L2_t oprot;
    net_L3_t otran;
    QString  oappl;                 /*!< NET processing library*/
    QString  oargs;                 /*!< NET additional arguments*/    
} smu_net_t;

typedef struct
{
    
    datalog_mode_t             datalog_mode; //Datalogging values
    QString         route;
    uint16_t        duration;

} smu_datalog_t;

/// ---------- SMU: common types section ----------

#define smu_Nch 8

/**
 * @brief Monintor readings
 *
 */
typedef struct
{
    double Ta;                      /*!< MON sensor temperatrure */
    double RH;                      /*!< MON sensor humidity */
} smu_mon_t;

/**
 * @brief SMU error code
 *
 */
typedef enum {
    SMU_NO_INIT = -2,
    SMU_FAIL    = -1,
    SMU_OK      =  0,
    SMU_IDLE    =  1,

    SMU_ERR_DRV_NOT_FOUND = 10,
    SMU_ERR_DRV_NOT_ASSOC = 11,
    SMU_ERR_DRV_NOT_CONF  = 12,
    SMU_ERR_DRV_MMAP_FAIL = 13,
    SMU_ERR_DRV_NO_SIGNAL = 14,
    SMU_ERR_DRV_NO_START  = 15,
    SMU_ERR_DRV_NO_STOP   = 16,

    SMU_ERR_LIB_NOT_FOUND = 20,
    SMU_ERR_LIB_NOT_RESOL = 21,

    SMU_ERR_NET_SERV_FAIL = 30,
    SMU_ERR_NET_PORT_FAIL = 31,

} smu_err_code_t;

/**
 * @brief SMU error state
 *
 */
typedef struct {
    smu_err_code_t svc;
    smu_err_code_t daq;
    smu_err_code_t dsp;
    smu_err_code_t net;
    smu_err_code_t mon;
} smu_err_t;

/**
 * @brief SMU multi-channel sample code
 *
 */
typedef struct {
    int16_t ch[smu_Nch];
} __attribute__((__packed__)) smu_mcsc_t;

/**
 * @brief SMU multi-channel sample real
 *
 */
typedef struct {
    float ch[smu_Nch];
} __attribute__((__packed__)) smu_mcsr_t;

/**
 * @brief SMU multi-channel frequency drift compensation values (new)
 *
 */
typedef struct {
    float gain;
    float offset;
} __attribute__((__packed__)) smu_compensation_t;


/**
 * @brief SMU multi-channel error compensation values (new)
 *
 */
typedef struct {
    smu_compensation_t ch[smu_Nch];
    smu_compensation_t f_drift;
} __attribute__((__packed__)) smu_mcse_t;

/**
 * @brief SMU multi-channel error compensation values
 *
 */
typedef struct {
    float k[smu_Nch];
} __attribute__((__packed__)) smu_mcsk_t;

typedef struct {
    float b[smu_Nch];
} __attribute__((__packed__)) smu_mcsb_t;

/**
 * @brief SMU multi-channel phasor reporting frame
 *
 */
typedef struct {
    qint64 t_stamp;
    smu_mcsr_t A;
    smu_mcsr_t P;
    smu_mcsr_t f;
    smu_mcsr_t df;
} __attribute__((__packed__)) dsp_phas_t;

/**
 * @brief SMU multi-channel sychronized rms-ac-dc measurement reporting frame
 *
 */
typedef struct {
    qint64 t_stamp;
    smu_mcsr_t rms;
    smu_mcsr_t dc;
    smu_mcsr_t ac;
} __attribute__((__packed__)) syn_meas_t;

/**
 * @brief SMU polar complex number notation
 *
 */
typedef struct {
    float mag;
    float ph;
} __attribute__((__packed__)) complex_polar_t;

/**
 * @brief SMU polar complex number notation
 *
 */
typedef struct {
    float re;
    float im;
} __attribute__((__packed__)) complex_rectangular_t;

/**
 * @brief SMU symmetrical components of a 3-phase system (either voltage or current)
 *
 */
typedef struct {
    complex_polar_t zero;
    complex_polar_t pos;
    complex_polar_t neg;
} __attribute__((__packed__)) symmetrical_components_t;

/**
 * @brief SMU symmetrical components reporting frame
 *
 */
typedef struct {
    qint64 t_stamp;
    symmetrical_components_t volt;
    symmetrical_components_t curr;
} __attribute__((__packed__)) symmetrical_volt_curr_t;

/**
 * @brief SMU kernel timestamp in seconds and nanoseconds
 *
 */
typedef struct {
    int64_t     tv_sec;
    long        tv_nsec;
} __attribute__((__packed__)) timespec64_t;

#endif // SUBTYPES_H
