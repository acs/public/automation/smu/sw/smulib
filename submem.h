// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SUBMEM_H
#define SUBMEM_H

#include <unistd.h>

#define PAGE_NUM    800
#define PAGE_SIZE   4096
#define MMAP_SIZE   (PAGE_SIZE*PAGE_NUM)

#include <QObject>
#include <QDateTime>

#include "subtypes.h"
#include "subcfg.h"

class subMEM : public QObject
{
    Q_OBJECT

    subCFG *smuCFG;

public:
    subMEM(subCFG *cfg, QObject* parent = nullptr);

    // DAQ Memory
    quint8 *daq_data;               // Address
    quint32 daq_pos;                // Position
    quint32 daq_inc;                // Increment (bytes)
    quint32 daq_dim;                // Dimension (samples)

    // DSP Memory
    quint8 *dsp_data;
    quint32 dsp_pos;
    quint32 dsp_inc;
    quint32 dsp_dim;

    timespec64_t time;

    QDateTime tref;                 // Time reference at PPS

    static void mcs_convert(smu_mcsc_t* code, smu_mcsr_t* real, smu_mcse_t* factors);
};

#endif // SUBMEM_H
