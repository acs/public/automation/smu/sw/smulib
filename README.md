# **smuLIB** <br/> _Shared libraries and common types_
Shared libraries for configuration and data types
smuDSP is a framework for developing signal processing plugins for [**smuSVC**](smusvc/README.md) daemon in the form of dynamic libraries.

## Architecture
The library exports four methods that are dynamically linked by the daemon smuSVC at run-time. The two methods _init()_ and _exit()_ are called when the library is loaded and unloaded, respectively. They are used therefore to allocate/initialize/destroy the variables used in the library. The method _proc()_ processes the raw data acquired by the board. It then packs and timestamps every batch of data. It is linked by a signal-slot mechanism to the smuNET library, which exposes a slot to to be informed when a new batch of processed data is available in the buffer. The method _test()_ can be used to provide self-test capabilties to the library.

```mermaid
stateDiagram
    direction LR
    [*] --> _init()
    _init() --> _proc()
    _proc() --> _exit()
    _exit() --> [*]
    state _proc() {
      direction LR
      state ready <<choice>>
      _test() --> ready
      smuDAQ --> ready : data_ready
      ready --> ready : t != nT
      ready --> smuNET : t = nT
    }
```

## Download
Create the folder **smuLIB** inside the project folder **smu**, then clone the repository inside.
```
cd ~/smu
mkdir smuLIB
git clone <repoLIB> smuLIB
```

## Usage
To use the newly built dynamic library at service startup, edit the SMU config file.
```
cd ~/smu/build
nano smuSVG.cfg
```

Change the parameters of the section **# DSP module** as needed, specifying the reporting rate *fr*, the *state* of each channel (0:_off_,1:_on_), the *alias* of each channel, and the DSP processing library *core* to be used (dsp\__\<newlib\>_).
```
# DSP module
fr:10
state:1,1,1,1,1,1,1,1
alias:V1,V2,V3,V4,I1,I2,I3,I4
core:dsp_newlib
```

To change the DSP library at run-time, the graphical interface smuGUI offers an easy configuration tool. Alternatively, it is possible to change the configuration by sending a simple text message to the SMU host on port 7080. The message payload is given by the previous configuration lines separated by **;** and enclosed in curly brakets **{...}**.
```
echo "{fr:10;state:1,1,...}" | nc host 7080
```
Not all the configuration entries need to be specified in the message, only those to be changed, for instance the DSP core.
```
echo "{core:dsp_newlib}" | nc host 7080
```

## Copyright
2017-2021, Carlo Guarnieri (ACS) <br/>
2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="LICENSE.md"><img alt="Apache License v2.0" style="border-width:0" src="docs/apache-2_logo.png" width="88"/></a><br />This work is licensed under the <a rel="license" href="LICENSE.md">Apache License v2.0</a>.

## Funding
<a rel="funding" href="https://hyperride.eu/"><img alt="HYPERRIDE" style="border-width:0" src="docs/hyperride_logo.png" height="63"/></a>&nbsp;
<a rel="funding" href="https://cordis.europa.eu/project/id/957788"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Hyperride" href="https://hyperride.eu/">HYbrid Provision of Energy based on Reliability and Resiliency via Integration of DC Equipment</a> (HYPERRIDE), a project funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/957788"> grant agreement No. 957788</a>.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Carlo Guarnieri Calò Carducci, Ph.D.](mailto:carlo.guarnieri@ieee.org)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)