// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "submem.h"

subMEM::subMEM(subCFG *cfg, QObject* parent)
    : QObject(parent)
{
    smuCFG = cfg;

    daq_pos  = 0;
    daq_dim  = 0;
    daq_inc  = 0;

    dsp_pos  = 0;
    dsp_dim  = 0;
    dsp_inc  = 0;

    daq_data = NULL;
    dsp_data = NULL;
}

void subMEM::mcs_convert(smu_mcsc_t* code, smu_mcsr_t* real, smu_mcse_t* factors)
{
    // Convert samples
    for (int i=0;i<8;i++)
    {
        real->ch[i] = ((float)code->ch[i])*factors->ch[i].gain+factors->ch[i].offset;    
    }
}