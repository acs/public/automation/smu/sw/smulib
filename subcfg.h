// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SUBCFG_H
#define SUBCFG_H

#include "subtypes.h"

#include <QCoreApplication>
#include <QObject>
#include <QDateTime>
#include <QDebug>
#include <QFile>

static const smu_daq_t daq_cfg_default = {FS_10kSPS, FB_100FPS, MODE_FREERUN, SYNC_PPS};
//const smu_dsp_t dsp_cfg_default = {FR_10PPS, {"1","1","1","1","1","1","1","1"}, {"V1", "V2", "V3", "V4", "I1", "I2", "I3", "I4"}, "dsp_raw"};
static const smu_dsp_t dsp_cfg_default = {FR_10PPS, {"1","1","1","1","1","1","1","1"}, {"V1", "V2", "V3", "V4", "I1", "I2", "I3", "I4"}, "dsp_ip-msdft"};
//const smu_net_t net_cfg_default = {"id", "", 7080, IPv4, TCP, "JSON", "", 7090, IPv4, TCP, "net_raw", ""};
static const smu_net_t net_cfg_default = {"id", "", 7080, IPv4, TCP, "JSON", "134.130.169.27", 1883, IPv4, TCP, "net_dft-mqtt", "user=roger,pass=password,pub=/test_pub,sub=/test_sub"};

#define SMU_CFG "smuSVC.cfg"
#define SMU_LOG "smuSVC.log"

#define SMU_LOGI(t,x) {qDebug().noquote() << QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzz") << " Info:    " << QStringLiteral(x).arg(t);}
#define SMU_LOGW(t,x) {qDebug().noquote() << QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzz") << " Warning: " << QStringLiteral(x).arg(t);}
#define SMU_LOGE(t,x) {qDebug().noquote() << QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzz") << " Error:   " << QStringLiteral(x).arg(t);}

class subCFG : public QObject
{
    Q_OBJECT
    QFile  *file;
    void init();

public:
    subCFG(QObject *parent = nullptr);

    QString config;
    smu_daq_t daq;
    smu_dsp_t dsp;
    smu_net_t net;
    smu_datalog_t datalog;
    smu_mon_t mon;
    smu_mcse_t p1;   //coefficients for parameter 1 compensation (e.g. Magnitude, RMS, etc.)
    smu_mcse_t p2;   //coefficients for parameter 2 compensation (e.g. Phase, DC, etc.)
    smu_mcse_t p3;   //coefficients for parameter 3 compensation (e.g. Frequency, AC, etc.)
    smu_mcse_t p4;   //coefficients for parameter 4 compensation (e.g. ROCOF, etc.)
    smu_err_t error;


public slots:
    void load();
    void save();
    void print();
    void toString(QString &str);
    void fromString(QString &str);
    void reportConf(QString &str);

signals:
    void updated();
};

#endif // SUBCFG_H
